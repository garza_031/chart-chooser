﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace ChartChooser
{
    public partial class MainPage : ContentPage
    {
        BarChart barChart = new BarChart();
        DonutChart donutChart = new DonutChart();
        RadialGaugeChart radChart = new RadialGaugeChart();

        public MainPage()
        {
            InitializeComponent();

            MainPicker.Items.Add("option 1");
            MainPicker.Items.Add("option 2");
            MainPicker.Items.Add("option 3");
            MainPicker.Items.Add("Choose one of the above");

            MainPicker.SelectedIndex = 3;

            MakeCharts();
        }

        private void MakeCharts()
        {

            var entries = new List<Entry>()
             {
                new Entry(100)
                {
                     Label = "test1",
                    Color = SKColor.Parse("#542356"),
                },

                new Entry(200)
                {
                    Label = "test2",
                    Color = SKColor.Parse("#212236"),
                },

                new Entry(300)
                {
                    Label = "test3",
                    Color = SKColor.Parse("#982856"),
                }

            };

            barChart = new BarChart() { Entries = entries };
           donutChart = new DonutChart() {Entries = entries };
            radChart = new RadialGaugeChart() { Entries = entries };


        }

     


        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
           
            
          switch (MainPicker.SelectedIndex)
            {
               case 0: charts.Chart = barChart;
                    break;
                case 1: charts.Chart = donutChart;
                    break;
                case 2: charts.Chart = radChart;
                    break;
                case 3:
                    break;
            }
        }
    }
}

